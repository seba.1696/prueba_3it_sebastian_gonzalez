import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//screens
import {Home, Prices, Indicator} from '../screens';

const RootStack = createNativeStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator initialRouteName="Home">
    <RootStack.Screen
      name="home"
      component={Home}
      options={{
        title: 'Indicadores',
        headerStyle: {
          backgroundColor: '#01163e',
        },
        headerTitleStyle: {
          fontWeight: '600',
          fontSize: 21,
          color: '#ffffff',
        },
        headerTitleAlign: 'center',
      }}
    />
    <RootStack.Screen
      name="indicator-prices"
      component={Prices}
      options={({route, navigation}) => ({
        headerTintColor: '#007da3',
        headerLeft: ({tintColor}) => (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <MaterialIcons
              name="arrow-back-ios"
              size={18}
              style={{marginLeft: 8}}
              color={tintColor}
            />
          </TouchableOpacity>
        ),
        title:
          route.params.unidad.replace('_', ' ').charAt(0).toUpperCase() +
          route.params.unidad.replace('_', ' ').slice(1),
        headerTitleStyle: {
          fontWeight: '600',
          fontSize: 21,
          color: '#01163e',
        },
        headerTitleAlign: 'center',
      })}
    />
    <RootStack.Screen
      name="indicator-detail"
      component={Indicator}
      options={({route, navigation}) => ({
        headerTintColor: '#007da3',
        headerLeft: ({tintColor}) => (
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <MaterialIcons
              name="arrow-back-ios"
              size={18}
              style={{marginLeft: 8}}
              color={tintColor}
            />
          </TouchableOpacity>
        ),
        title:
          route.params.unidad.replace('_', ' ').charAt(0).toUpperCase() +
          route.params.unidad.replace('_', ' ').slice(1),
        headerTitleStyle: {
          fontWeight: '600',
          fontSize: 21,
          color: '#01163e',
        },
        headerTitleAlign: 'center',
      })}
    />
  </RootStack.Navigator>
);

const Navigation = () => (
  <NavigationContainer>
    <RootStackScreen />
  </NavigationContainer>
);

export default Navigation;
