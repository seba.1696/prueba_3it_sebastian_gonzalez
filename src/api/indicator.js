import axios from 'axios';

import {HOST} from './service.env';

export const getIndicators = () => {
  return axios.get(`${HOST}/`);
};

export const getIndicatorByType = type => {
  return axios.get(`${HOST}/${type}`);
};

export const getIndicatorByDate = (type, date) => {
  return axios.get(`${HOST}/${type}/${date}`);
};
