import {useState, useEffect} from 'react';

export const useFetch = (api_url, call, setCall) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      await api_url()
        .then(({status, data}) => status === 200 && setResponse(data))
        .catch(e => setError(e))
        .finally(() => {
          setLoading(false);
          setCall(false);
        });
    };

    if (call) fetchData();
  }, [call]);

  return {response, error, loading};
};
