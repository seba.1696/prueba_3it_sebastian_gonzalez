import React, {useState} from 'react';
import {
  ActivityIndicator,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import DatePicker from 'react-native-date-picker';

export const Detail = ({data, date, setDate, loading}) => {
  const [open, setOpen] = useState(false);

  const closeDateModal = () => setOpen(false);
  const openDateModal = () => setOpen(true);

  const onchangeDate = value => {
    let date = new Date(value);
    closeDateModal();
    setDate(
      `${date.getDate()}-${
        date.getMonth() < 10
          ? '0'.concat(date.getMonth() + 1)
          : date.getMonth() + 1
      }-${date.getFullYear()}`,
    );
  };

  if (loading) {
    return (
      <ActivityIndicator
        size="large"
        color="#01163e"
        style={{marginVertical: 16}}
      />
    );
  }

  return (
    <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
      <DatePicker
        modal
        mode="date"
        open={open}
        date={new Date()}
        onCancel={closeDateModal}
        onConfirm={onchangeDate}
        locale="es"
      />
      <View style={styles.actual_value_container}>
        <Text style={styles.actual_value_text}>
          {`$ `.concat(
            data && data.serie && data.serie.length ? data.serie[0].valor : ``,
          )}
        </Text>
      </View>
      <View style={styles.indicator_values_container}>
        <View style={styles.indicator_item}>
          <Text style={styles.indicator_item_text}>Nombre</Text>
          <View style={styles.indicator_item_value_container}>
            <Text style={styles.indicator_item_value}>{data.nombre}</Text>
          </View>
        </View>
        <View style={styles.indicator_item}>
          <Text style={styles.indicator_item_text}>Fecha</Text>
          <TouchableOpacity
            style={styles.indicator_item_value_container}
            onPress={() => openDateModal()}
            disabled={true}>
            <Text style={styles.indicator_item_value}>{date}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.indicator_item}>
          <Text style={styles.indicator_item_text}>Unidad de Medida</Text>
          <View style={styles.indicator_item_value_container}>
            <Text style={styles.indicator_item_value}>
              {data.unidad_medida}
            </Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 16,
    paddingHorizontal: 20,
  },
  actual_value_container: {
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  actual_value_text: {
    fontSize: 30,
    fontWeight: '600',
    color: '#007da3',
  },
  indicator_values_container: {
    flexDirection: 'column',
  },
  indicator_item: {
    flexDirection: 'row',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 10,
  },
  indicator_item_text: {
    color: '#000000',
    fontWeight: '500',
    fontSize: 14,
  },
  indicator_item_value_container: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderWidth: 1,
    borderColor: '#00000030',
    borderRadius: 4,
    minWidth: 100,
  },
  indicator_item_value: {
    textAlign: 'right',
  },
});
