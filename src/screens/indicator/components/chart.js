import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Dimensions, View} from 'react-native';
import {LineChart} from 'react-native-chart-kit';

export const Charts = ({data, loading}) => {
  const [labels, setLabels] = useState([]);
  const [prices, setPrices] = useState([]);

  useEffect(() => {
    if (data && data.serie) {
      data.serie.forEach((element, index) => {
        if (index <= 9) setPrices(prev => [element.valor, ...prev]);
      });

      data.serie.forEach((element, index) => {
        if (index <= 9) {
          setLabels(prev => [
            `${new Date(element.fecha).getDate()}/${
              new Date(element.fecha).getMonth() < 10
                ? '0'.concat(new Date(element.fecha).getMonth() + 1)
                : new Date(element.fecha).getMonth() + 1
            }`,
            ...prev,
          ]);
        }
      });
    }
  }, [data]);

  if (loading) {
    return (
      <ActivityIndicator
        size="large"
        color="#01163e"
        style={{marginVertical: 16}}
      />
    );
  }

  return (
    <View
      style={{
        backgroundColor: '#fff',
        paddingHorizontal: 5,
        justifyContent: 'center',
        flex: 1,
      }}>
      <LineChart
        data={{
          labels: labels,
          datasets: [
            {
              data: prices,
            },
          ],
        }}
        width={Dimensions.get('window').width * 0.975} // from react-native
        height={Dimensions.get('window').height * 0.35}
        yAxisLabel="$"
        chartConfig={{
          backgroundColor: '#e26a00',
          backgroundGradientFrom: '#01163e',
          backgroundGradientTo: '#007da3',
          decimalPlaces: 2,
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
          labelColor: opacity => `#fff`,
          style: {
            borderRadius: 8,
          },
          propsForDots: {
            r: '5',
            strokeWidth: '1',
            stroke: '#49cdf5',
          },
        }}
        bezier
        style={{
          marginVertical: 8,
          borderRadius: 8,
        }}
      />
    </View>
  );
};
