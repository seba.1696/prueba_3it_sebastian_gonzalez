import React, {useEffect, useState} from 'react';
import {View} from 'react-native';

import {Detail} from './components/detail';
import {Charts} from './components/chart';

import {useFetch} from '../../hooks/useFetch';
import {getIndicatorByType, getIndicatorByDate} from '../../api/indicator';

export const Indicator = props => {
  const unidad = props.route.params.unidad;
  const base_date = new Date();

  const [date, setDate] = useState(
    `${base_date.getDate()}-${
      base_date.getMonth() < 10
        ? '0'.concat(base_date.getMonth() + 1)
        : base_date.getMonth() + 1
    }-${base_date.getFullYear()}`,
  );
  const [call, setCall] = useState(true);
  const [onlyCall, setOnlyCall] = useState(true);

  const {
    response: indicator_today,
    loading: loading_indicator,
    error: error_indicator,
  } = useFetch(() => getIndicatorByDate(unidad, date), call, setCall);
  const {
    response: indicator_range,
    loading: loading_indicator_range,
    error: error_indicator_range,
  } = useFetch(() => getIndicatorByType(unidad), onlyCall, setOnlyCall);

  useEffect(() => {
    setCall(true);
  }, [date]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <Detail
        data={indicator_today}
        loading={loading_indicator}
        date={date}
        setDate={setDate}
      />
      <Charts loading={loading_indicator_range} data={indicator_range} />
    </View>
  );
};
