import React, {useState} from 'react';
import {ActivityIndicator, Text, View} from 'react-native';

import {PricesList} from './components/list';

import {useFetch} from '../../hooks/useFetch';
import {getIndicatorByType} from '../../api/indicator';

export const Prices = props => {
  const [refres, setRefresh] = useState(true);
  const {response, error, loading} = useFetch(
    () => getIndicatorByType(props.route.params.unidad),
    refres,
    setRefresh,
  );

  const onRefresh = () => setRefresh(true);

  if (loading) {
    return (
      <ActivityIndicator
        size="large"
        color="#01163e"
        style={{marginVertical: 16}}
      />
    );
  }

  return (
    <PricesList
      data={response && response.serie ? response.serie : []}
      onRefresh={onRefresh}
      refreshing={loading}
    />
  );
};
