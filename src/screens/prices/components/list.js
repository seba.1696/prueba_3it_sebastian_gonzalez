import React from 'react';
import {FlatList, RefreshControl} from 'react-native';

import {ItemPricesList} from './item';

export const PricesList = ({data, onRefresh, refreshing}) => {
  return (
    <FlatList
      data={data}
      keyExtractor={item => item.fecha}
      renderItem={({item}) => <ItemPricesList item={item} />}
      refreshControl={
        <RefreshControl
          tintColor="#01163e"
          colors={['#01163e']}
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }
    />
  );
};
