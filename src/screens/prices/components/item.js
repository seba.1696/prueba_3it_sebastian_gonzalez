import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export const ItemPricesList = ({item}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text_date}>
        {new Date(item.fecha).toLocaleDateString('es-Es')}
      </Text>
      <Text style={styles.text_value}>{`$ `.concat(item.valor)}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#70707040',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingVertical: 8,
    paddingRight: 64,
  },
  text_date: {
    color: '#007da3',
    fontSize: 14,
    fontWeight: '400',
  },
  text_value: {
    color: '#000000',
    fontSize: 16,
    fontWeight: '600',
    lineHeight: 16,
  },
});
