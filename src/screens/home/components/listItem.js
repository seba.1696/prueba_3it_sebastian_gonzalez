import React from 'react';
import {StyleSheet} from 'react-native';
import {Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const Item = ({item, navigation}) => {
  const goToTPrices = unidad =>
    navigation.navigate('indicator-prices', {unidad});
  const goToIndicator = unidad =>
    navigation.navigate('indicator-detail', {unidad});

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.inticator}
        onPress={() => goToTPrices(item.codigo)}>
        <Text style={styles.indicator_text}>
          {item.codigo.replace('_', ' ')}
        </Text>
        <Text style={styles.indicator_unit}>{item.unidad_medida}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.indicator_detail}
        onPress={() => goToIndicator(item.codigo)}>
        <Feather name="info" size={21} color="#007da3" />
        <MaterialIcons
          name="arrow-forward-ios"
          size={14}
          style={{marginLeft: 8}}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#70707040',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  inticator: {
    flex: 1,
  },
  indicator_text: {
    fontSize: 16,
    fontWeight: '700',
    color: '#1b1c1c',
    lineHeight: 32,
    textTransform: 'capitalize',
  },
  indicator_unit: {
    fontSize: 12,
    fontWeight: '500',
    color: '#007da3',
    textTransform: 'capitalize',
  },
  indicator_detail: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
});
