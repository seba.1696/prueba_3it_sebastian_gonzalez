import React from 'react';
import {FlatList, RefreshControl, View} from 'react-native';
import {Item} from './listItem';

export const HomeList = ({data, onRefresh, refreshing, navigation}) => {
  return (
    <View style={{flex: 1}}>
      <FlatList
        data={data}
        keyExtractor={item => item.codigo}
        renderItem={({item}) => <Item item={item} navigation={navigation} />}
        scrollEnabled={true}
        refreshControl={
          <RefreshControl
            tintColor="#01163e"
            colors={['#01163e']}
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
      />
    </View>
  );
};
