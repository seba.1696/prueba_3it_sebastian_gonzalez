import React, {useEffect, useState} from 'react';
import {ActivityIndicator} from 'react-native';

import {HomeList} from './components/list';

import {useFetch} from '../../hooks/useFetch';
import {getIndicators} from '../../api/indicator';

export const Home = props => {
  const [refres, setRefresh] = useState(true);
  const [data, setData] = useState([]);

  const {response, error, loading} = useFetch(
    getIndicators,
    refres,
    setRefresh,
  );

  const onRefresh = () => setRefresh(true);

  useEffect(() => {
    setData([]);
    if (response) {
      setData(
        Object.keys(response).map(i => {
          if (typeof response[i] === 'object') return response[i];
        }),
      );
    }
  }, [response]);

  if (loading) {
    return (
      <ActivityIndicator
        size="large"
        color="#01163e"
        style={{marginVertical: 16}}
      />
    );
  }

  return (
    <HomeList
      {...props}
      data={data && data.filter(data => data !== undefined)}
      onRefresh={onRefresh}
      refreshing={loading}
    />
  );
};
