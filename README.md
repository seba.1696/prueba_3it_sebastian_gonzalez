<h1 align="center">Prueba 3IT</h1>

<h3>Sebastián Alberto González Burdiles</h3>

<h5>Aplicación solo probada en emulador/dispositivo Android</h5>

## Get Started

### Installation

#### Android

Para ejecutar la aplicación es necesario tener configurado un emulador android
(<a href="https://reactnative.dev/docs/environment-setup">Configuración</a>),
y/o tener activadas las opciones de desarrollador y la depuración USB en un dispositivo móvil.

Instalar librerias: npm install <br/>
Ejecutar: npx react-native run-android
